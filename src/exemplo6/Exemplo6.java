/**
 * Exemplo de Produtor/Consumidor
 * sem sincronizacao
 * 
 * Autor: Reinaldo Fernandes
 * Ultima modificacao: 16/08/2017
 */
package exemplo6;

import static java.lang.Thread.sleep;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Exemplo6  {

    
    public static void main(String [] args) throws InterruptedException{
        
        //cria novo pool de threads com duas threads
        ExecutorService pool = Executors.newCachedThreadPool();
        
         int numThreads = 5;
        
        //cria UnsynchronizedBuffer para armazenar ints
        Buffer [] buffer = new BufferSincronizado[numThreads];
        
        for(int i=0; i<numThreads; i++){
            buffer[i] = new BufferSincronizado();
        }
        
        System.out.println("Ação\t\tValor\tIndiceBuffer\tValor");;
        System.out.println("-------\t\t-------\t--------------\t------------\n");
        
        System.out.println(buffer.length);
        
        //Executa Producer e Consumer, fornecendo-lhes acesso ao mesmo Buffer->UnsynchronizedBuffer: sharedLocation
        pool.execute( new Producer( buffer ));
        
        for(int i=0; i<numThreads; i++){
            pool.execute( new Consumer( buffer, i ));   
        }
                
                    
        pool.shutdown();
    }//fim main
    
}//fim classe
